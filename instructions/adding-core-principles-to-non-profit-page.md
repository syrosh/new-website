## Adding core principle to non profit page

1. The core principles are managed by the yml file in the directory `_data/non-profit.yml`
1. To add a principle edit the file and add a new value like shown below.

**Important:** In Yaml it is important to keep the indenting the same otherwise it will not work.
```
- title: No zero days
  text: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris arcu elit, molestie eu malesuada ac, lobortisvel nulla. Pellentesque tincidunt odio condimentum augue lacinia, non varius nisi auctor.
```